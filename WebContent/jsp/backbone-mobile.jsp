<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<script src="../js/jquery-1.7.2.js" ></script>
<script src="../js/handlebars-v1.3.0.js" ></script>
<script src="../js/underscore.js" ></script>
<script src="../js/backbone.js" ></script>
<link rel="stylesheet" href="../css/jquery.mobile-1.4.2.min.css">
<script src="../js/jquery.mobile-1.4.2.js"></script>
<script id="ContactListTemplate" type="text/x-handlebars-template">
    {{#each contacts}}
        <li data-role="list-divider">{{this.name}}</li>
        <li>
            {{#each this.phone}}
                <p>phone : {{this.number}} </p>
            {{/each}}
        </li>
    {{/each}}
</script>
<!-- End templates setup -->
<script>
     
    // Stub out the person model
    var Contact = Backbone.Model.extend({});
     
    // Create a collection of persons
    var ContactList = Backbone.Collection.extend({
        model: Contact,
        url:"../"
    });

    // Define the view for People
    var ContactListView = Backbone.View.extend({
        render: function() {
            // This is method that can be called once an object is init. You could
            // also do this in the initialize event
            var source = $('#ContactListTemplate').html();
            var template = Handlebars.compile(source);
			// note: "{contacts: this.collection.toJSON()}" is used to make handlerbar and backbone compatible
            var html = template({contacts: this.collection.toJSON()});
            $('#contactListView').html("");
            $('#contactListView').append(html);
            $('#contactListView').listview('refresh');
        },

        initialize: function(){
            this.collection.on('add', this.render, this)
        }
    });

    // Create instance of People Collection
    var contacts = new ContactList();
    
    var contactsView = new ContactListView({
        collection: contacts
    });
    
    contacts.fetch({
        success: function(returnObj, response) {
            contacts = returnObj;
            contactsView.render();
        }
    });
    
    $(document).ready(function(){
    });
    
    function addNewContact(){
        var contactName = $('#contactName').val();
        var contantNumber = $('#contantNumber').val();
        var contactExt = $('#contactExt').val();
        var newContact = { name: contactName, order: -1, phone : [{number: contantNumber, ext: contactExt}] };
        contacts.create(newContact);
        contactsView.render();
    }
    
</script> 
</head>
<body>
    <div data-role="header">
        <h1>Quick Contact</h1>
    </div>
    <ul id="contactListView" data-role="listview" data-inset="true"></ul>  
    <br/><br/>
    <div style="padding: 10px;">
        <h3>Add New Contact</h3>
        <form id="addContactForm" action="" method="PUT">
            <label for="text-3">name</label>
            <input id="contactName" data-clear-btn="true" name="name" id="text-3" value="" type="text">
            <label for="text-3">phone</label>
            <input id="contantNumber" data-clear-btn="true" name="phone" id="text-3" value="" type="text">
            <label for="text-3">ext</label>
            <input id="contactExt" data-clear-btn="true" name="ext" id="text-3" value="" type="text">
            <input value="Submit" type="button" onclick="addNewContact();">
        </form>
    </div>
    
</body>
</html>