<%--

    DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.

    Copyright (c) 2010-2013 Oracle and/or its affiliates. All rights reserved.

    The contents of this file are subject to the terms of either the GNU
    General Public License Version 2 only ("GPL") or the Common Development
    and Distribution License("CDDL") (collectively, the "License").  You
    may not use this file except in compliance with the License.  You can
    obtain a copy of the License at
    http://glassfish.java.net/public/CDDL+GPL_1_1.html
    or packager/legal/LICENSE.txt.  See the License for the specific
    language governing permissions and limitations under the License.

    When distributing the software, include this License Header Notice in each
    file and include the License file at packager/legal/LICENSE.txt.

    GPL Classpath Exception:
    Oracle designates this particular file as subject to the "Classpath"
    exception as provided by Oracle in the GPL Version 2 section of the License
    file that accompanied this code.

    Modifications:
    If applicable, add the following below the License Header, with the fields
    enclosed by brackets [] replaced by your own identifying information:
    "Portions Copyright [year] [name of copyright owner]"

    Contributor(s):
    If you wish your version of this file to be governed by only the CDDL or
    only the GPL Version 2, indicate your decision by adding "[Contributor]
    elects to include this software in this distribution under the [CDDL or GPL
    Version 2] license."  If you don't indicate a single choice of license, a
    recipient has the option to distribute your version of this file under
    either the CDDL, the GPL Version 2 or to extend the choice of license to
    its licensees as provided above.  However, if you add GPL Version 2 code
    and therefore, elected the GPL Version 2 license, then the option applies
    only if the new code is made subject to such option by the copyright
    holder.

--%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
  <head>
    <title>ajax test</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
    /**
     * ajax http send method
     * @param url url
     * @param action CRUD ex. GET, POST, PUT, DELETE
     * @param response callback method
     */
    function ajaxHttpSend(url, action, type, response)  
    {  
        var ajaxurl = url;
        $.ajax({
            type: action,
            dataType: type,
            error: function(x, status, error) { 
                alert("An ajax error occurred: " + x + " : " + status + " Error: " + error);
            },
            url:ajaxurl,
            success:function(result){
                // display result
                if (type == 'JSON') {
                    document.getElementById('result').innerHTML = JSON.stringify(result);
                } else if (type == 'XML') {
                    document.getElementById('result').innerHTML = '<code>' + result + '</code>';
                    //document.getElementById('result').innerHTML = jQuery(result).text();
                } 
                // do something
                response(result)
            }
        });
    }
       
    function callback(responseText) {
        alert(responseText);
    }
    </script>
  </head>
  <body>
      <br/><br/>
      <h3><a href="javascript:void(null);" onclick="ajaxHttpSend('../', 'GET', 'JSON', callback);">JSON list</a></h3>
      <h3><a href="javascript:void(null);" onclick="ajaxHttpSend('../', 'GET', 'XML', callback);">XML list</a></h3>
      <div id="result"></div>
  </body>
</html>
