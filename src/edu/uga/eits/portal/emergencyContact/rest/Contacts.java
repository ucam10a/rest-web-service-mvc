package edu.uga.eits.portal.emergencyContact.rest;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

import javax.inject.Singleton;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.glassfish.jersey.server.mvc.Template;

import com.google.gson.Gson;

import edu.uga.eits.portal.emergencyContact.orm.Phone;
import edu.uga.eits.portal.emergencyContact.orm.QuickContact;

@Path("/")
@Singleton
@Template
@Produces("text/html;qs=5")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Contacts {
    
    private Map<Integer, QuickContact> contacts = new TreeMap<Integer, QuickContact>();
    
    private Collection<QuickContact> contactList;
    
    public void updateContacts() throws Exception {
        contacts = ContactService.getContactCache();
        contactList = contacts.values();
    }
  
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.TEXT_XML})
    public Contacts getContactXML() throws Exception {
        updateContacts();
        return this;
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JsonArray getContactJSON() throws Exception {
        updateContacts();
        JsonBuilderFactory factory = Json.createBuilderFactory(null);
        JsonArrayBuilder contactsBuilder = factory.createArrayBuilder();
        for (QuickContact contact : contactList){
            JsonArrayBuilder phoneBuilder = factory.createArrayBuilder();
            for (Phone p : contact.getPhone()){
                JsonObjectBuilder builder = Json.createObjectBuilder();
                builder.add("number", p.getNumber());
                builder.add("ext", p.getExt());
                phoneBuilder.add(builder);
            }
            JsonObjectBuilder c = Json.createObjectBuilder();
            c.add("order", contact.getOrder());
            c.add("name", contact.getName());
            c.add("phone", phoneBuilder);
            contactsBuilder.add(c);
        }
        JsonArray list = contactsBuilder.build();
        return list;
    }
    
    @Path("order/{order}/")
    public QuickContact getContact(@PathParam("order") int order) throws Exception {
        updateContacts();
        QuickContact contact = contacts.get(order);
        return contact;
    }
    
    @POST
    @Consumes("application/json")
    public void addContact(JsonObject contact) throws Exception {
        String json = contact.toString();
        Gson gson = new Gson();
        QuickContact c = gson.fromJson(json, QuickContact.class);
        String name = c.getName();
        String phone = "";
        String ext = "";
        for (Phone p : c.getPhone()){
            phone = p.getNumber();
            ext = p.getExt();
        }
        ContactService.addContact(name, phone, ext);
        ContactService.updateContactCache();
    }

    public void setContacts(Map<Integer, QuickContact> contacts) {
        this.contacts = contacts;
    }

    // used to render jsp
    public Collection<QuickContact> getContactList() {
        try {
            updateContacts();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return contactList;
    }

    public void setContactList(Collection<QuickContact> contactList) {
        this.contactList = contactList;
    }

} 